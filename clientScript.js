(function () {
  var size = '@page {size: landscape}';

  var pageStyleSheet = this.document.createElement('style');
  pageStyleSheet.setAttribute('type', 'text/css');
  pageStyleSheet.appendChild(this.document.createTextNode(size));
  this.document.head.appendChild(pageStyleSheet);

  var fontsLink = document.createElement('link');
  fontsLink.rel = 'stylesheet';
  fontsLink.type = 'text/css';
  fontsLink.href = 'fonts/stylesheet.css';
  document.head.appendChild(fontsLink);

  window.onload = function () {
    if (window.location.search.includes('__print=true')) {
      setTimeout(function () {
        window.print();
        window.close();
      }, 1000);
    }
  }
})();
