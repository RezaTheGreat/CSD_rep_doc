# CSD Reporting Engine


# Table of Contents

- [CSD Reporting Engine](#csd-reporting-engine)
- [Table of Contents](#table-of-contents)
    - [Installation and deployment](#installation-and-deployment)
        - [Environment Setup](#environment-setup)
    - [Report Design Environment](#report-design-environment)
        - [Left Section](#left-section)
        - [Center Section](#center-section)
            - [Views](#views)
        - [Lower](#lower)
    - [Report Structure](#report-structure)
        - [1. Project](#1-project)
        - [2. Report File](#2-report-file)
- [Report Design](#report-design)
- [Expressions](#expressions)
    - [Expression ‌Builder](#expression-%E2%80%8Cbuilder)
            - [top menu bar](#top-menu-bar)
            - [lower section](#lower-section)
            - [Categories](#categories)
    - [Data Sources](#data-sources)
    - [Applica REST Data Source](#applica-rest-data-source)
    - [Data Sets](#data-sets)
        - [Changing Data set data types](#changing-data-set-data-types)
        - [Data Set property bindings](#data-set-property-bindings)
    - [Laying out the report](#laying-out-the-report)
        - [General report elements](#general-report-elements)
        - [Data](#data)
            - [Binding](#binding)
                - [Editing Data Bindings](#editing-data-bindings)
        - [Tables](#tables)
            - [Inserting tables](#inserting-tables)
            - [Table Rows](#table-rows)
            - [Selecting the table](#selecting-the-table)
            - [Adding more rows to the table](#adding-more-rows-to-the-table)
            - [Table Row Number](#table-row-number)
            - [Add more data to the table](#add-more-data-to-the-table)
            - [Edit data](#edit-data)
            - [Table Groups](#table-groups)
            - [Elements General Properties](#elements-general-properties)
            - [Table page breaks](#table-page-breaks)
                - [Dealing with grouped table page breaks](#dealing-with-grouped-table-page-breaks)
- [Styling](#styling)
    - [Creating Styles](#creating-styles)
    - [Importing Styles](#importing-styles)
- [Libraries](#libraries)
    - [Adding items to Libraries](#adding-items-to-libraries)
    - [CSD sales reports library](#csd-sales-reports-library)
- [Scripting](#scripting)
    - [CSD reports client script](#csd-reports-client-script)
    - [External JavaScript Libraries](#external-javascript-libraries)
        - [Using Decimal.js library in CSD sales reports](#using-decimaljs-library-in-csd-sales-reports)
- [Report Parameters](#report-parameters)
    - [Usage](#usage)
    - [Using Parameters in CSD reports](#using-parameters-in-csd-reports)
    - [Visibility](#visibility)
- [Report Master Page](#report-master-page)
    - [CSD Master Page](#csd-master-page)
- [Data Aggregation](#data-aggregation)
    - [BIGINTSUM Aggregation](#bigintsum-aggregation)
- [Running the report](#running-the-report)
    - [Report Outputs](#report-outputs)
- [Rendering Reports out of the eclipse designer](#rendering-reports-out-of-the-eclipse-designer)
    - [Installing Apache Tomcat](#installing-apache-tomcat)
    - [installing BIRT viewer](#installing-birt-viewer)
    - [Viewing a report in BIRT viewer](#viewing-a-report-in-birt-viewer)
    - [BIRT viewer in CSD reports](#birt-viewer-in-csd-reports)

The Business Intelligence and Reporting Tools (BIRT) Project is an open source software project that provides reporting and business intelligence capabilities for rich client and web applications. 
BIRT has two main components: a visual report designer within the Eclipse IDE for creating BIRT Reports, and a runtime component for generating reports that can be deployed to any Java environment. The BIRT project also includes a charting engine that is both fully integrated into the report designer and can be used standalone to integrate charts into an application.
## Installation and deployment
BIRT uses eclipse for report design, which doesn't need installation, however a ready-made package including the additional extensions is provided. in order to start using it, extract it and run `eclipse.exe`
### Environment Setup
eclipse has __"perspectives"__ for different kind of usages to set the visible options, for report design select it's perspective from _Window_ drop-down menu:

        Window → Perspective → Open Perspective → Report Design

## Report Design Environment
after you set the perspective for report Design, the Eclipse environment gets the following order:


![alt text](\Images\env.png "Report Design Environment")

### Left Section
in the left there are two group of tabs on each other.

        1. Palette: displays all the elements that you can place in a report.
        2. Data Explorer: Houses all the data elements that is used in the current report.
        3. Resource Explorer: it shows the resources that your Project has. like the report files, scripts, ...
        4. Navigator: it shows the directory structure of your project. 
        5. Outline: shows the outline of your current report. all the elements in your report are populated here and can be edited directly from here.
in the center you can see the main report layout, with different views of the report tabs under it.

### Center Section
in the center you see the current views. Layout, Master Page, Script, XML Source.

#### Views
 
 1. Layout: the main default view is the layout which report elements are visually designed.
 1. Master Page: used to design the Master Page of the report
 1. Scripts: is used to edit the event script of the current selected element]
 1. XML Source: shows the underlying XML source of the report
### Lower

1. Property Editor: edits the properties of the currently selected element
1. Error log: show the errors found in the report
1. Problems: show the detected problems when debugging

## Report Structure
A BIRT Report design is actually a XML file saved in `_.rptdesign` format that is then interpreted by the _BIRT Viewer_ to the correct representation of your report.
A (multipage) report has two main components, a Master Page and a Layout. A Master page is the layout that is repeated in every page and contains header/footer, title, page number, etc. that is repeated or consistent through the whole report, while the actual report content is in the layout section.
every report is designed separately, every designed report element can be stored in a file called _library_ to be added in each report. for CSD reports, there a special library curated for the consistency in reports and ease in the design process.
### 1. Project
Eclipse organizes files by projects. You can create one project to organize all your reports or create multiple projects to organize your reports by categories. For each project that you create Eclipse creates a directory in your file system.                                    

        File → New → Project
### 2. Report File
BIRT Provides report templates but for our specific purpose we start with a blank one to design it from scratch.

        File → New → Report
# Report Design
we go through the general procedure of report design and then we introduce report designing using the CSD library which is provided for consistency between reports.
1. Preparing Data
1. Adding elements
1. Attributing Styles
1. Running the report

first we introduce the expressions as they are used in almost every section of the report. you can see them in Data, Aggregations, scripts, filters, ...

# Expressions
You can create many reports using data that comes directly from a data source simply by dragging the data set fields from Data Explorer to the report. Sometimes, however, you want to display information that is not in the data source, or you want to display data differently from the way it appears in the data source. You might also want to sort data using a formula, rather than sorting on an existing field. For these cases, and many others, write expressions using JavaScript.

An expression is a statement that produces a value. An expression can be a literal value, such as:

`3.14`

`"It is easy to create reports with BIRT"`

When you drag a field into the report, BIRT Report Designer creates a column binding with the correct expression. The expression specifies the name of the field from which the re‍‍‍‍‍port displays values. For example, the following expressions get values from the customerName field and the phone field, respectively:

‍‍‍‍‍‍`dataSetRow["customerName"]`

`dataSetRow["phone"]`

An expression can contain any combination of literal values, fields, operators, variables, and functions that evaluates to a single value. In the following examples, the first expression combines static text with a field, the second uses a JavaScript function, and the third multiplies the values of two fields:

`"Order Total: " + row["orderTotal"]`

`row["orderDate"].getYear()`

`row["itemQuantity"] _ row["itemPrice"]`


## Expression ‌Builder
for making and editing expressions BIRT offers a feature-rich component that can be used for making, editing and checking the syntax of expressions. You can find it by clicking on the ![alt text](\Images\expression_btn.png "Expression Editor Button") Button. The Expression Builder window opens and you can see the various available options offered of editing the expressions that we are reviewing thoroughly.

![alt text](\Images\expression_builder.png)

there is the main expression editor in the middle with a menu bar above and below it. you can write a script to make data bindings the way you want. it offers to insert all available items in the report. such as data source items, previously made data bindings, standard JavaScript functions, BIRT functions, report parameters and so on. we go through the environment and explain items as necessary.
#### top menu bar

![alt text](\Images\expression_editing_options.png "basic editing options") : **Basic Editing Options:** Copy-Cut-Paste-Delete-Undo-Redo

![alt text](\Images\validate.png "validate script") : **Validate:** this option validates your script with JavaScript syntax. use after writing your script to check the syntax.

![alt text](\Images\calendar.png "Calendar") : **Calendar:** this option offers a calendar where you can pick dates from to be in your script. pick your desired date and click on OK to insert it in your script.

#### lower section

![alt text](\Images\operators.png "Operators") : Operators. valid operators to insert in your script.

in the lower section we can see the object browser, we first select the category followed by the sub category and then the corresponding item.

#### Categories
- __Available data sets:__ all the datasets that are present in the report. if you use an item from the datasets and make a new expression based on it, it will become a new binding and can be used later in in the _Available column bindings_ section later.
- __Available column bindings:__ data that are currently bound to the report items when you select this option, the report elements that currently contain data bindings are shown in the subcategory so the bindings can be used. **Note:** the table row number lies under this category when you select a table element in report.
- __Report Parameters:__ defined report parameters can be used in the expression script.
- __Native JavaScript functions:__ this option offers the currently available JavaScript functions that are a part of the language natively. such as `Math, Number, JSON`. when you select the function, their available properties are shown.
- __BIRT functions:__ these are JavaScript functions that are offered by BIRT that can be useful in many situations like finance and string comparison functions.
- __Operators:__ this contains operators that are not present in the operators menu bar.

after you find your desired item, double click on it to be inserted on the current cursor position in the Expression Field. after that click on *Validate* button to see if there are any errors in the script. if there are any errors in the script, the information section above the top menu bar informs you about them.
the optimum approach is that if you want to make a new expression, you first look for your desired item in the _Available column bindings_ category to avoid making unnecessary extra bindings, and then if you didn't find your binding, go for it in the _Available data sets_ section.

for example if you want to insert a row number expression like this

                row number: 1

you should write following expression: 

![alt text](\Images\row_number.PNG "custom row number script")

## Data Sources
To access data for a BIRT report, you create and use a BIRT data source. A BIRT data source is an object that contains the information to connect to an underlying data source. Each type of data source requires different connection information. Use Data Explorer, to create and manage BIRT data sources.

## Applica REST Data Source
as new CSD (sales) systems use RESTful API calls to the service for data and a REST data source was not provided out of the box, a special Data Source for BIRT is crafted, compiled and deployed in the provided Eclipse package which is called _"Applica Rest Data Source"_.
this data source enables us to use RESTful API calls as the queries for data used in reports.
to add a Data source.

to add a data source to the report, choose Data Explorer, If you use the default report design perspective, Data Explorer is to the left of the layout editor, next to Palette.
 
Right-click Data Sources, then choose New Data Source from the context menu. New Data Source displays the types of data sources you can create.

![alt text](\Images\Applica.png "Applica Rest Data source")

Choose Applica REST Data Source and a name for your data source and Click Next then Finish.

A ready REST Data source is in the CSD Library named _rest Data Source_ 

## Data Sets
A data set is an object that defines all the data that is available to a report. To create a data set, you must have an existing BIRT data source. As with data sources, BIRT Report Designer provides wizards to create data sets. The first and the only required step in creating a data set is to select the data to retrieve from a data source. After this first step, you can optionally process the raw data as needed for the report.
for example if your report connects to a JDBC data source, you use a SQL `SELECT` statement to specify the data to retrieve. 
In Data Explorer, right-click Data Sets, and choose New Data Set from the context menu. Choose your data source (rest Data Source) and a name for your data set. then Click next.

![alt text](\Images\Dataset.png "Creating new data set")

setting Query (API call)

![alt text](\Images\apicall.png "setting the query text")

after clicking Finish, the Data Source editing Window Appears. here we can edit various aspects of data. 

![alt text](\Images\edit_datasource.png "edit data source")

### Changing Data set data types

for formatting purposes we should change the data type of all currency data to _Decimal_.
click on _Output Columns_ and double click on the desired data types to change their type.

![alt text](\Images\change_type.png "change data types")

### Data Set property bindings

as Portions of the Query are set in the Application/Server side and are parameterized we facilitate BIRT report parameters (More On this later) within our data set query.

![alt text](\Images\property_binding.png "property binding")

## Laying out the report

now with our dataset prepared, we can now use elements such as tables to lay out our report. here are general report elements available:

### General report elements
1. Label: display static text in report
1. Text: display text with HTML formatting applied on
1. Dynamic Text: to display CLOB text data with formatting
1. Data: Dataset column or expression
1. Image: an Image from a file or a URL
1. Table: represent data in a table
1. List: just like a table but with more flexible formatting
1. Chart: show data using charts
1. Cross tab: represent data in a matrix like format
1. Grid: is used to visually organize data
the notable elements are discussed further in detail.

### Data
every set of data that comes from the data source is called a dataset row and the expression describing its name is called a data column. in order to show data, a data column must be bound to a visual element like a table cell. 

#### Binding
Data can be bound to some report elements. e.g tables need a data source to be bound or they cause an error when the report is run.

![alt text](\Images\binding.png "dataset bound to a table")

##### Editing Data Bindings
![alt text](\Images\edit_data_binding.png "edit a data binding")

for editing the data bindings, you can either double click on them or select them and click edit. the Edit Data Binding Window opens. you can edit various properties of data and in the expression section, you can edit the expression that represents data using javascript syntax. more on the Expression builder later. 

if you edit the data binding and it's being used multiple times in the report e.g different table cells or in other data expressions, a prompt appears asking that if you want to change the binding for as it is being used or if you want to make a new one by changing the current properties. 


![alt text](\Images\binding_changed.png "change binding for more than one cell")


if you click No, your existing data binding changes as well as the one being used in the report and if you click Yes, a new data binding with your fresh settings will be made and used instead of the old one.
### Tables
A table provides the following functionality:
- Iterates through all the data rows that a data set returns
- Enables you to lay out data easily in a row and column format.
tables can be laid out to the slightest detail in each cell to represent the data in the desired form. in order to data be shown in the tables, a data set must be bound to the table.
#### Inserting tables
to insert table to the report drag it from the Palette and drop it to the layout. Insert Table prompts you to specify the number of columns and detail rows to create for the table. The dialog also prompts you to select a data set to bind with the table. after you select the data source, the available columns are shown to select from. after selecting the desired columns, an appropriate table is inserted, however you can edit it to suit your needs.

![alt text](\Images\insert_table.png "insert table using dataset data")

after the table is inserted you can see the table cells populated with the labels as column headers and table detail cells filled with data  elements as their titles are wrapped in `[]`.

![alt text](\Images\table_header_detail_1.png "table detail and headers")
![alt text](\Images\table_header_detail_2.png "table detail and headers")

#### Table Rows

as you can see, the first row is the header, which is consistent throughout the report and appears above the table in each page in multipage report,

the second row is the detail row, which data appears in and is generated for every dataset row.

the last row, the footer row is inserted at the end of the table where dataset rows are finished.
#### Selecting the table
in order to edit the table, you should first select it, when your mouse pointer is in the layout area, the table indicator is shown below the table, click on it to select the the table. when the table is selected the markers are shown on each row and column to be selected individually and edited.

![alt text](\Images\select_table.png "table indicator")
![alt text](\Images\table_header_detail_2.png "selected table")

#### Adding more rows to the table
the available number of each kind of rows are not limited, e.g when your header/footer spans more than one row for styling purposes you can add as many of them as possible. to add more rows first select the table then right click on the desired row to add a row Above or Below it as follows.

                Right Click on table → Insert → Row → Above(Below)
![alt text](\Images\add_detail_row.png "add detail row")

#### Table Row Number
the original BIRT `row.__rownum` binding starts from zero. if there is a case that row number should be used it has to be used incremented by one to be correct.
`row.__rownum +1` 


#### Add more data to the table
if you insert a table that has empty cells e.g by not providing a dataset in Insert Table prompt, you can drag the data from dataset Data Explorer and drop it in to the desired cell

![alt text](\Images\drag_drop_data.png "drag data from dataset")


#### Edit data
you can edit the representation of the data, e.g adding suffix/prefixes to it. to do so double click on a data on the report. the Edit Data Binding Window opens. you can edit various properties of data and in the expression section, you can edit the expression that represents data using javascript in the expression builder.
#### Table Groups
BIRT has the ability to group table rows on a certain data row. in default situation when no groups are defined in the table, rows are gathered from datasource and rendered equally in the table. when you define a group, you set a dataset row to group on and then rows are first grouped and will get a header and a footer for each group. also you can edit page break settings for groups individually.

to insert a new group, select table's detail row and right click on it and then select Insert Group → Above.

![alt text](\Images\insert_group.png "Insert Group window")

here we go through the notable options:
- **Name:** you must define a name for the group.
- **Group On:** this option defines the dataset row that grouping is applied on, the group key. either one of the available rows can be selected or Expression Builder can be used to make a new binding and grouping be applied on that.
- **Interval:** depending on the data type of the row that grouping is being applied on, you can set the grouping to be applied on. for example you can set the interval for a year if the data type is Date.
- **Range:** if you select an interval, you can define a range for it. e.g every two years if you selected interval to year.
- **Page Break:** you can set a different page break for the group than the table
- **Repeat Header:** if checked, the header is repeated after each page break.
- **Filters And sorting:** you can add a filter to the group like a table to omit the rows when a certain expression evaluates to *true*. and you can sort group rows like a table using a sorting key.

When you Click OK, you see a header and footer inserted to your table with the Group On row being in the header row. you can now edit the header and footer as you want. when you insert items in the group header/footer, the data row corresponding to your group key appears there.


to delete a row, click on its marker to select it and then press `Del` key or right click and select _Delete_. if your table hasn't any headers or rows, when you right click on it, there are options to add one.
#### Elements General Properties
when you select an element, it's properties appears in the property editor and properties tab is selected. most important and useful properties are:

- **General:** Name: you can set a name for the element, typography settings such as font,...., Style: assign a CSS style rule that is defined in the report or imported from the libraries. Display: set html display property
- **Padding**, Margin, Border styling properties of the element. More on Styling later.
- **Format Number:** formatting of number displayed, regarding the selected locale. predefined formats can be used and customized or the _Custom_ option could be selected to allow further customization of the numbers displayed.
- **Format Date:** formatting of the Date/Times shown in the element.  predefined formats can be used and customized or the _Custom_ option could be selected to allow further customization of the Date/Times displayed.
- **Format String:** used to turn all the letters to upper/lower case for the strings. more formatting can be applied by selecting the _Custom_ option.
- **Page Break:** this option defines what happens when the number of table detail rows reach the end of the page. this option will be discussed later in detail.
- **Visibility:** defines the visibility of the element. the elements can be hidden in defined situations. this option will be discussed later in detail.
- **Advanced:** this options lists all the available options that are not covered in basic previous sections and gives the ability to edit them directly.
- **Comments:** you can set comments of the element here.

---

#### Table page breaks
When the number of rendered dataset rows exceeds a certain amount e.g one page, a page break occurs and the next row is rendered in the next page. the behavior of making page breaks can be set. the settings are almost the same between the different elements. select the element and form the lower section select page break to see the available options.

![alt text](\Images\pageBreak.png "Table Page Break Settings")

there are three main section for page break to be applied:
- Before: Before the element. this determines if a page break occurs before the rendering of the element.
- After: is to add a break after the element is rendered or not.
- Inside: to add a page break inside an element or not.
- Master Page: to set the master page when a page break occurs. you can select one or none of your report master pages.
- *Page Break Interval:* this a setting specific to the tables. here, the amount of pages to render before a page break occurs can be set. to keep your tables aligned perfectly between the pages, you can set the table row width to a static value and calculate the page break interval based on it to fill the whole page and not going outside of one page.

*THIS NUMBER ONLY APPLIES ON THE DETAIL ROWS AND *NOT* ON THE HEADER/FOOTERS. IF THE TABLE HAS GROUPS INSIDE IT, THE GROUP HEADER/FOOTER IS NOT CALCULATED IN THIS. TO KEEP TRACK OF TOTAL RENDERED ROWS IN THIS SITUATION, SCRIPTS ARE USED.*

three main settings:
- Always: when set a page break is always added in the following place.
- Auto: When set, if there is not enough room for the element to render in the current page, a break is added in the following place, otherwise it will render completely in the current page without adding a break.
- Avoid: if a page break is set to avoid, BIRT fully renders the element in the current page regardless of other settings.


##### Dealing with grouped table page breaks
as the table page breaks only contain the detail rows in their calculation, it can lead to a table with incorrect page break settings and sometimes one table exceeding one page when rendered. to avoid this, scripting comes handy. in order to overcome this, there must be a counter increasing by each row, whether it's a group header/footer or a detail row and since we have determined the height of the row in the styling, when the counter reaches the certain amount that is the maximum rows before the table height exceeds one page, a page break must be inserted before the next row is rendered. and the counter must be set to zero in each page.

first we define the variable to be zero in the `onInitialize` event of the report. to do this click on a an empty area on the report and then select the script tab and then pick the `onInitialize` script and add the following code. 

                 rowCount=0;



![alt text](\Images\onInitialize.png "report on initialize")

select the  `onPageStart` script and add the previous code to make the variable zero on the page start too

![alt text](\Images\OnPageStart.png)

select the group header and go to the `onCreate` script and add the following code:
                
                rowCount ++ ;

![alt text](\Images\groupHeaderOnCreate.png)

repeat this step for all of the group header/footer rows. to count them as the actual rows inside the tables.

for the detail row, use this code in the onCreate Script

```
rowCount++;
if (rowCount >=YourDesiredPageBreakInterval) {
	this.getStyle().pageBreakBefore="always";
}
```

here `YourDesiredPageBreakInterval` stands for the amount of detail rows you want to safely show in the table. and the `this.getStyle().pageBreakBefore="always";` sets the current element's *Before* page break property (detail row here) to always. so when the next row is created, it will have the Page Break Before property of *Always*.

for example if your detail row height is *1 cm* and you set the page break interval to 12, your table will be *12 cm* high ***not*** regarding the table header and footer. in report design you should take the header and footer of the both page and table into consideration.


for the inner grouping footers it is better to set the increment of rows regarding the number of the table groups. for example if you have two groups inside each other it's better to set the row increment of the inner group to 2. and use the `rowCount+=2;` code in their on create script. that's done to avoid a new outer header being rendered at the end of the page without any detail rows after it.

![alt text](\Images\innerGroupFooterScript.png "out group footer script")

here we got the our intended behavior: we counted all the rows including group header / footers , and set the page break on our intended interval with the groups considered. also we prevented the outer group header being rendered at the end of the page without any detail rows after it. 

**THE VALUES SET AT THE PROPERTIES SECTION ARE IN THE MAIN XML REPORT FILE AND HAVE PRIORITY OVER THE SCRIPTS** so when the scripts at the `onCreate` run and the rows got rendered in page, the page break setting of the row is *overridden* by the original table page break setting that is set through the layout editor.



# Styling
BIRT offers great styling features. you can style each element individually or define a style to use it multiple times for the several report elements. a particular style also can be exported to a library for being available to use between multiple reports.

the basic styling of elements can be done in the General section of the property editor however you can make a styling rule (simply called a style) to use on multiple elements.

## Creating Styles
 
In the layout editor, select the report element to which you want to apply a style. To create a style but not apply it to any elements, click in an empty area on the report page
Choose Element→Style→New Style. New Style appears.The left side displays the property categories. The right side displays the properties for the category that you select.

![alt text](\Images\newStyle.png "Style Editor")


Specify one of the following settings:

- To apply style properties to a specific type of report element, select Predefined Style, and select a style from the drop-down list.
- To create a user-named style, select Custom Style, and specify a unique descriptive name. Ensure that the name is not the same as any of the predefined style names. If you specify a name that is the same as a predefined style, your custom style takes precedence, and you can no longer use the predefined style to apply cascading styles. 
Set the desired style properties by selecting a property category on the left and specifying property values.

When you finish setting style properties, choose OK to save the style. If you selected an element before you created the style, BIRT Report Designer applies the style to that element. 


The custom styles and predefined styles that you define appear in Outline. Any time you want to change a style, access it from this view.

![alt](\Images\styles.png "Report styles")

you can edit styles either by right clicking the in report and select Style→ Edit Style and then selecting the style you want to edit or accessing it in the outline view.

## Importing Styles
Styles can be imported from the CSS files too. to import one Select the layout editor. 

Choose Element→Style→Import CSS Style. 

In Import CSS Styles, in File Name, specify the name of the CSS file from which to import styles. Choose Browse to find the file. Import CSS Styles displays all the styles defined in the CSS file.  

![alt](\Images\import_styles.png)

Select the styles that you want to import. To import all the styles, choose Select All.

When you finish making your selections, choose Finish. BIRT Report Designer copies the styles to the report. The imported styles appear in the Styles list in Outline. 

The custom styles and predefined styles that you define appear in Outline, as shown in Figure 6-2. Any time you want to change a style, access it from this view.

![alt](\Images\styles.png "Report styles")

# Libraries

 A single report developer with a requirement for only a few reports can use can design report individually and effectively. For a larger project, either one with more developers or one that requires more reports, many designs need to use the same elements or layouts.

To support creating and maintaining standard formats, building reports in collaboration with other report developers, and avoiding error-prone, repetitious design tasks, BIRT uses the following file types:

* Library
 
The main purpose of a library is to provide developers of report designs with a shared repository of predesigned report elements and styles. The file-name extension for a library file is *.rptlibrary*. BIRT locates libraries in the resource folder.
A library is a dynamic component of a report design. When a library developer makes changes to a library, the report design synchronizes itself with the changed library. In this way, changes propagate easily within a suite of report designs.
A library stores customized report elements, such as data sources, visual report items, styles, and master pages. Use a library in a report design to access the customized elements. You can use multiple libraries in a single report design. By using multiple libraries, you can separate the styles and functionality that different projects or processes need.
* Template
 
 The main purpose of a template is to provide a standard start position for a new report design. As such, the structure of a template file is identical to the structure of a report design file. The file-name extension for a template file is .rpttemplate. BIRT locates templates in the template folder.
 A template is a static framework on which to build a new report design. A report design derived from a template modifies a copy of that template. For this reason a report design can derive from only one template. Because report designs use copies of the template, when a template developer changes a template, report designs based on that template do not automatically reflect those changes.
 A template provides a structure for a standard report layout. A template can contain visual report items that appear in the report’s layout, data sources and data sets, and master page layouts. A template uses libraries in the same way that a report design does. BIRT Report Designer provides a set of standard templates, such as Simple Listing and Grouped Listing report templates. 
* A CSS file

This type of file provides styles for formatting items in a report, similar to the formatting of items on a web page.

## Adding items to Libraries
Creating an element in a library uses the same wizards and dialogs as creating an element in a report design. After you add an element to a library, you make changes to it in the same way as you do in a report design. then right click on it and select *Export to library* a prompt opens for you to set a new name for the item in the library. if none, the default name will be chosen for the item in the library. then you can select the library you want to add the item in or make new one if there aren't any.

In the resource explorer you can see your libraries and their items beneath it

![alt](\Images\libraryResourceExplorer.PNG "table is added to the \'lib\' library").

## CSD sales reports library

CSD sales reports use a library that contains the following options:
* Master Page
this is an A4 landscape master page with right to left orientation and *10 mm* borders from each side with following header and footer specifications:

the header is *40 mm* high and is a grid containing following items:
- Report title
- Company name
- Company address
- Company logo
- Report description/date span.

the footer is *15 mm* high and is a grid containing contains following items:
- Report Date label
- Report Date in Jalali format
- A place for the the reporting operator to sign
- Page number and total report pages

this is the starting point for the most CSD sales reports.

* Styles

there are styles in the CSD sales report library used to style the common report elements.
- table_portrait: main styling for portrait tables. sized at *220 x 100 mm* with can shrink property set to true in order to let the cells resized to regarding their content.
- table_landscape: main styling for portrait tables. sized at *130 x 100 mm* with can shrink property set to true in order to let the cells resized to regarding their content.
- row_header: table header styling rule. *5 mm* high and with solid *2 mm* borders around it.
- row_footer: table footer styling rule. *8 mm* high and with solid *2 mm* borders around it.
- row_group_header: group headers styling rule. same as the group_header but with a gray background to highlight the groups
- row_group_footer: group footers styling rule. same as the group_footer to highlight the groups.
- row_footer_spanner: this style rule is used to span an invisible in the end the tables to prevent the actual last row span the remaining page room. it has no borders, colors, etc. and is *100% high*.
- row_detail: table detail row styling rule. it has *2 mm* borders around the row.
- cell_header : table header cell styling rule. *2 mm* borders, central alignment and *Far.Nazanin* bold font sized at *10 pt*.
- cell_detail: table detail cell base style. *2 px* borders, central alignment and *Far.Nazanin* font.
- cell_currency: the styling rule for currency table cells. this styling has the special formatting for the currency number values and should be used in currency cells.
- company_information: special styling rule for the company information section in the master page.
- date: special styling for the report creation date that is inserted in the footer of each page or any dates used in the reports

# Scripting
BIRT provides a powerful scripting capability that enables a report developer to create custom code to control various aspects of report creation. every element has its own events where the scripts can be put and executed. some elements have their own specific events. BIRT provides several JavaScript classes in addition to the native JavaScript classes that are a part of Rhino JavaScript. A report developer can access any of the BIRT classes and their properties and functions when writing an expression. The BIRT classes appear in the expression builder’s Category list as BIRT Functions. 
here we review the common generic ones. to see the available events for each element, select an them and click on the script tab. 

![alt text](\Images\scriptEvents.png "available events")

- onPrepare: Executes during the setup of the element. you can change the design of the element in this event where the data bindings are not available yet.
- onCreate: this script is executes when the element is prepared and created but not rendered yet.
- onRender: Script executed when the element is prepared for rendering in the Presentation engine.
- onPageBreak: this script executes for all report items on a page when a page break occurs.
- initialize: this script executes at the beginning of the report generation.
- ClientScripts: this script runs on the client browser.

## CSD reports client script
In order to maintain the style in multiple clients without needing to install the fonts individually, fonts are loaded once when the CSD sales report is run. to achieve this the fonts are in a separate directory within the reports directory and contain a CSS file describing them. to add the style sheet relations are added to the report through the ClientScript script. the client script reference is save as *clientScript.js* file. 

various actions can be performed through JavaScript in these events altering the design and layout of the report such as changing the stylings, page breaks, data bindings etc. almost everything that is in the main XML file can be altered via scripting.

## External JavaScript Libraries
In addition to executing JavaScript within a report, BIRT provides the option to use external JavaScript files located in the resource folder. Use the resources View and the property editor to associate these files with the selected report. For example, an external JavaScript file has the following content:

                function getMyValue( ){
                        return "This is a test of the add js button";
                }

If this file is located in the resource folder, a report developer can select the resources tab of the property editor and add the JavaScript file to the report. The JavaScript file is not imported, but is referenced in the report. After making this association, the developer can call `getMyValue( )` in any BIRT expression or any of the event handler scripts.
### Using Decimal.js library in CSD sales reports
When the calculations grow large in financial items in order to maintain the accuracy and precision of the calculation made in the reports and due to JavaScript's native functions inaccuracy in large number an External JavaScript for decimal arbitrary calculations is used. the [Decimal JS](https://github.com/MikeMcl/decimal.js) library is an arbitrary-precision Decimal type for JavaScript. there is a minimal version called `decimal.min.js` that is used in CSD reports.

to use it in reports you should download the file and put it in your current report directory. then select the report (click on an empty area in the report) and click on the *Resources* in the property editor. scroll down to *JavaScript Files* and Click on **Add Files** to add the file to your report.

![alt text](\Images\Resources.PNG "Resources and Decimal JS added to the report")

now the functions provided by the library can be used in the expressions/scripts.

![alt text](\Images\decimalJSUse.png)

in CSD sales reports calculations regarding the large financial values are done using the **DecimalJS** library.

![alt text](\Images\decimal1.png)

![alt text](\Images\decimal2.png)

![alt text](\Images\decimal3.png)

![alt text](\Images\decimal4.png)



----------------------------------------------------------------------------------------------------------------------------------------------------------------------------

# Report Parameters
When you create a report, you build a data set and typically specify filter criteria to display a certain set of data in the report. When a user views the report, the user sees the information that you selected. As users become familiar with the report and recognize its potential as an analytical tool, they may want to view the data in different ways. For example, in a sales report, a user may want to view only sales in a particular region, or sales over a certain amount, or sales that closed in the last 30 days.

The solution for this type of ad hoc reporting requirement is for the report to prompt the user to provide information that determines what data to display. You make this solution available by creating report parameters.

for example they are used in the queries to make the `WHERE` clause based on the current report running situation. you first define and configure them and they will become available in the expression builder in the *Report Parameters* category. and they will be used to prompt the user to specify values that determine what rows to retrieve.
to add a report parameter to your report in Data Explorer, right-click Report Parameters, and choose New Parameter.


![alt text](\Images\new_parameter.png "new report parameter")

let's review the parts of the window
**Name:** a name must be provided for the parameter. It is good practice to use a prefix, such as RP for report parameters in the name to help distinguish report parameters from other parameter types.

**Data type:** you can set the parameter's datatype just like other data types. The data type that you select for the report parameter determines the formatting options that are available if you choose to provide a default value or a list of values for the report parameter. The data type of the parameter does not have to match the data type of the field in the data source. Values in an orderID field, for example, can be stored as integers in the data source, but the report parameter that is associated with this field can be of string type

**Display type:** parameter's prompt can be shown in a few different ways. Textbox where users have to enter the parameter value manually, and in Combobox, ListBox, RadioButton you can provide the user with multiple options to choose from which can be either static where you define the options manually or coming from data source rows. each item can has its own Display text and value. when you select to populate options Dynamically, you can chose from the available dataset rows.
in list box you can allow user to select multiple options or not. 

*Note:* when you choose Radio button and you want to populate its options from the datasource, you can not use the dataset rows directly, each item should be imported using the import value button where you can choose individually from dataset rows.

![alt text](\Images\radiobox_parameter.png "Radio box parameter")

**Display As:** in this section you define how the parameter is shown to the user. Help text is the text being shown to the user when the report is run and user needs to enter it.
**Format As:** you can define the formatting of the prompt text here and when you click on *Change...* button the format builder window opens and you can use from multiple formatting options available. then you can see the parameter with the formatting applied on it in the preview section..
**CheckBoxes** the parameters can be set in many ways. either stopping the report generation and forcing the user to enter/choose it or silently being given/set on the design time or being neglected if not provided.
- **is Required:** if this option is checked the report won't run if the parameter isn't supplied and you'll see an error.
- **Hidden:** this option implies that there will be no prompts asking user for the parameter. this is useful when you are supplying parameters silently.
- **Do Not Echo Input:** for text box parameters this option turns off the input echo for the user and user won't see what they are typing.
**Default Value** the default value of the parameter if there are none supplied.

## Usage
when you want to use a report parameter, you can select report parameters in categories and available parameters are shown. the syntax is `params["<parameter name>"]`. double click on them to be inserted in the expression editor. note that the `*.value` property of the parameters actually contains the parameter value.

![alt text](\Images\report_params_expression.png "selecting form report parameters")

![alt text](\Images\param_binding.png "service and token are report parameters")

## Using Parameters in CSD reports
in CSD (sales) reports there are a few report parameters that are common between reports and are used in almost all of them in dataset queries in their property binding. they are gathered in library and could be added in any new reports in future.
- **service:** since the reporting server address can be set within the app, service parameter is used to imply that and is added at the beginning of queries. this property is required.
- **token:** the report engine gets token from the client and sends it to the report server. this property is required.
- **date_from:** it determines the starting date to report from. this parameter is used in the report layout.
- **date_to**: it determines the last date that report data is coming from.  this parameter is used in the report layout.


## Visibility
Visual report elements can be hidden in certain defined conditions. suppose that you have a report parameter named `RP_HidePageNo` and you want to hide the page number when you generate the Web output. it is easily done in BIRT. you hide that element using its visibility property.

![alt text](\Images\visibility.png "Visibility Property")

Select *Hide Element* to specify that this element be hidden. If you want the element, such as an empty row, to always be hidden, this selection is all you need to do. To hide the element conditionally, specify the condition as well. you can specify the output format of report that the condition applies on. To apply the hide condition for all report formats, select For all outputs. To apply the hide condition for certain report formats, select For specific outputs. Also select this option to apply different conditions, depending on the report format. 

Specify the hide condition by clicking the ![alt text](\Images\expression_btn.png "Expression Editor Button") button. the expression builder opens and you can edit the expression as always. 
 
In the expression builder, create an expression that specifies the hide condition. Remember, think about when to **HIDE** the element, not when to display it. For example, if you want to hide a table row when the price is below 250000, conditionally hide the row element using the following expression:
 
`row["price"] < 250000`

This expression hides the table row when price amount is less than 250000.

you can test it by running the report and see if your element is hidden when the hide condition is met.

# Report Master Page
As mentioned before every report consists of a layout and Master Page. in master page the actual "page" that the report data layout comes on top of is designed.
You can design a page layout that enhances the report’s appearance and usability. A page layout specifies the style of the pages that display report data. For example, you can specify that all pages display the report title in the top left corner, the company logo in the top right corner, a page number in the bottom right corner, and a report-generation date in the bottom left corner. You can also design special effects, such as a watermark that appears in the background. To design a page layout, you should customize the report’s master page.

your master page settings are consistent throughout the whole report and report layouts inherit them. however you can override them when you set them in your reports

to begin in the layout editor, choose the Master Page tab. The layout editor displays the master page. The palette displays additional elements under AutoText, specifically for use in the master page.

![alt text](\Images\masterPage_properties.png "Master Page General Properties")

in general master page properties you can set the header and footer attributes like their dimension. You set the paper size of your report, the direction of the report to be either Right-to-Left or Left-To-Right here. in border settings you can set the border of your report pages. 

as mentioned before you can see new items under the *Auto text* section in the pallette that you can use only in Master page. e.g total pages. you can use them like data element in your reports.

you can have more than one master page in a report. when you add a master page from the library in order to use it you should delete the old provided one.

you can put normal report elements like grids, tables as well as the auto text ones.
## CSD Master Page
there is a ready made Master Page for CSD (sales) reports that which is put in the library. it contains the essential header and footer elements: Report title, report time span, Company information, total report pages, report creation date, signature place. 


---

# Data Aggregation
One of the key features of any report is the ability to display summary, or aggregate, information. For example, a sales report can show the overall sales total; sales subtotals by product type, region, or sales representatives; average sales figures; or the highest and lowest sales figures.

Aggregating data involves performing a calculation on a set of values rather than on a single value. For a simple listing report, aggregate calculations are performed on values in a specific field, over all the data rows in the report.

For example, BIRT calculates an average payment in a report by adding the values in the Amount field in every row, then dividing the total by the number of rows. 

an aggregation is treated like a data element. when you create one it gets bound to the parent element (like a table) and creates a new data binding. 
to insert an aggregation, in the palette drag an aggregation in drop it in your desired place, like a table cell. the aggregation builder window opens and has following options.


![alt text](\Images\aggregation.png "aggregation builder")

- Column Binding Name: As the name implies, it's the name of the data binding that is created for this aggregation.
- Display Name: The name that is Displayed in the layout editor. It will be same as the Binding name if none provided. 
- Data type: the data type of aggregation.
- Function: the aggregation function to be applied. like SUM, AVG , ...
- Expression: the dataset row for aggregation to be applied on. you can open the expression builder to build on expression for the aggregate to be applied on.
- Filter Condition: if this expression is set, the aggregation only applies if the filter expression evaluates to true and will be ignored if it evaluates to false. you can edit it using the expression builder button.
- Aggregate On: the aggregation can be applied on either the whole table or the group. if you select the the group, the aggregation is calculated for each group separately.

## BIGINTSUM Aggregation
Due to Javascript basic types losing precision in large numbers and In order to maintain to accuracy when the numbers in aggregation grow large, need of a new aggregation function without losing precision was felt. BIRT's extensibility allows us to make new aggregations to suit our needs. so a new open source  Aggregation called **_BIGINTSUM_** is made and is already installed in the BIRT package provided. the source is hosted on [GitHub](https://github.com/RezaTheGreat/BIRT-Big-Int-Sum-Aggregation) and can be reviewed.

in order to use it, you can select it in the Aggregation list just like the normal SUM. 

![alt text](\Images\bigintsum.png)


_BIGINTSUM IS NOT A STANDARD OUT OF THE BOX BIRT AGGREGATION FUNCTION, IN ORDER TO USE IT THE COMPILATION FROM THE SOURCE AND INSTALLING IS NEEDED TO APPEAR IN THE AGGREGATION LIST AND FUNCTION. ONLY THE SUPPLIED BIRT PACKAGE ALREADY HAS IT._ 

# Running the report
after laying out the report we can run it, at get the final results in various formats. to run the report Click on the ![alt text](\Images\run_btn.png "run") button. it default to the Web Viewer. it opens your browser and shows a paginated report with basic controls to navigate. More on report output/views later.
## Report Outputs
BIRT is packed with various output options out of the box and it's extensibility allows you to build new ones. if you click on the drop down menu next to the output button, you'll see the current output options.
 

![alt text](\Images\view_report.png "add detail row")

the difference between the web viewer and HTML output is the Web Viewer is your report generated and paginated and is ready for printing while in the HTML output you'll all your report material as a single webpage. for example you see your table as a one single table with all the rows in it without any pagination performed on it.

# Rendering Reports out of the eclipse designer
in order to use the designed reports in your application, the BIRT viewer comes in to action. BIRT viewer is the engine that parses the report files in to meaningful reports as designed in the viewer. it is a J2EE servlet that can be deployed to either *Apache Tomcat* ro *JBOSS*. The BIRT Web Viewer is a web application, comprised of servlets and JSPs, that encapsulates the BIRT Report engine API to generate reports. In addition to generating reports.
## Installing Apache Tomcat
In order to run Apache Tomcat, Java Runtime Environment must be installed on your system. to install go to https://java.com/download and download and install the version that suits your system. in Windows JRE must be added in you PATH environment variable.
to install Tomcat go to http://tomcat.apache.org under the download section go for the Tomcat 8.5 and download one of the *Core* versions of your choice. you can run tomcat as a system service on Windows if you download and install the *Windows Service Installer* package. after installing tomcat you can proceed to installing birt viewer itself.

 ## installing BIRT viewer
BIRT Report Engine includes the BIRT report viewer as a web archive (.war) file and as a set of files and folders. Deploying the BIRT report viewer requires copying files from BIRT Report Engine, which you must install separately from BIRT Report Designer.

You must place the BIRT report viewer in a location where Apache Tomcat can access it. Typically, this location is the `$TOMCAT_INSTALL/webapps` directory. On restarting Apache Tomcat, the application server automatically recognizes and starts the BIRT report viewer application if the BIRT report viewer is in this folder.

`$TOMCAT_INSTALL`: installation directory of the Tomcat

`$BIRT_RUNTIME`: the extracted directory of birt

To install the BIRT report viewer as an application in a file system folder, use the *WebViewerExample* folder in the BIRT Report Engine installation.
1. Navigate to `$TOMCAT_INSTALL/webapps`.
2. Create a subdirectory named `birt`.
3. Copy the web viewer example directory and all its subdirectories to this new folder, as illustrated by the following DOS command:
`xcopy /E "$BIRT_RUNTIME/WebViewerExample" $TOMCAT_INSTALL/webapps/birt`
basically you copy the `WebViewerExample` to `webapps` folder and rename it to birt.
4. If the BIRT reports need additional JDBC drivers, add the JAR files for the JDBC drivers to the following directory:
`$TOMCAT_INSTALL/birt/WEB-INF/platform/plugins/org.eclipse.birt.report.data.oda.jdbc_<version>/drivers`
5. Restart Apache Tomcat.

you can see more about deploying BIRT viewer on [Deploying BIRT Reference](http://otadocs.opentext.com/documentation/ihub3-dev/AIG/aig14/index.html)

## Viewing a report in BIRT viewer
BIRT engine has a two-phase report generation process. The first phase, called Generation, takes the report design `(.rptdesign)` and generates a report document `(.rptdocument)`, which holds the report data and design. The second phase is the Presentation phase, in which the report document is rendered in HTML, paginated HTML, or PDF. The Viewer servlet wraps the engine functionality and exposes the newer functions to the end user. This architecture also allows the engine to support Table of Contents (TOC) functionality and CSV exporting, without rerunning the report. 

The Viewer servlet implements two mappings. The first is `"/run"` which combines the Generation and Presentation phases into one operation. This mapping should be used when generating PDF documents, or when additional functions such as Page Navigation, TOC, Export to CSV, etc., are not needed. The second is `"/frameset"` which was added to facilitate the new Viewer functions, and divides the Generation and Presentation phases.
After deploying the BIRT report viewer to your J2EE container *(Tomcat here)*, you can use the two available BIRT report viewer servlets to access your BIRT reports using a web browser. To view a BIRT report using a browser, use a URL in one of the following formats, where parameter_list is a list of URL parameters:

`http://localhost:8080/birt/run?__report=<report file path>&parameter_list`

`http://localhost:8080/birt/frameset?__report=<report file path>&parameter_list`

The run and frameset servlets display reports in two different ways. The run servlet displays the report as a stand-alone web page or a PDF file. If the report requires parameters, specify them in the URL. The frameset servlet displays a page in the browser with a page navigation toolbar and buttons to do the following tasks:
* Display a table of contents.
* Display a parameters dialog.
* Display a dialog for exporting data.
* Display a dialog for exporting the report in various formats.
* Print the report.
* Print the report to a document file on the server.

## BIRT viewer in CSD reports
in CSD (sales) platform, the report viewer component is contained in a portable *Tomcat* server which does not need any installation. the only prerequisite is the JRE to run the tomcat itself. the viewer components such as the icons and the texts are customized to maintain the look consistency of the application.

the *report-engine* directory in CSD sales application root directory is actually a *Apache Tomcat 8.5.23 Windows x64* package with a working iteration of BIRT viewer deployed in the webapps ROOT of the the reporting engine. when the main CSD sales application runs, it fires up the 'catalina.bat' which is the main starting point of Tomcat in Windows. 

in `report-engine\webapps\ROOT\webcontent\birt\ajax\ui\app\BirtNavigationBar.js` file, in line 20 the `_IMAGE_EXTENSION` property is set to `.svg` since the customized icons are in svg format.

the images and icons are located in `report-engine\webapps\ROOT\webcontent\birt\images` folder. the `Loading.gif` is changed with a custom image and the customized images for navigation buttons are in `.svg` format.

the navigation icons tooltip are changed in `report-engine\webapps\ROOT\webcontent\birt\pages\control\NavigationbarFragment.jsp` and the progress and cancel button string in progress dialog are changed to their Persian equivalents.

2017 - 2018 Reza Kashani reza.arani@outlook.com, then reports specialist of CSD Co.